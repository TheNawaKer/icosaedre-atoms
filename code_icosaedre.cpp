#include <iostream>
#include <GL/glew.h>
#include <GL/glut.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <math.h>
#include <iostream>
#include <fstream>

#include "shaders_utilities.hpp"
#include "lecture_trajectoire.hpp"
#include "icosaedre.hpp"

#define GLM_FORCE_RADIANS
// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
using namespace glm;

using namespace std;

// Pour la fenêtre
GLsizei width = 500;
GLsizei height = 500;

GLfloat* sommets;
GLfloat* couleurs;

float stepTrans=1.0;
int mouseXOld, mouseYOld;
bool leftbutton = false;
bool rightbutton = false;
bool middlebutton = false;

GLuint vboid[2];

GLuint programID;

GLuint MatrixID;
GLuint positionID;
GLuint colorID;

glm::mat4 Projection;
glm::mat4 View;
glm::mat4 Model;
glm::mat4 MVP;

glm::mat4 translation;
glm::mat4 trans_initial;
glm::mat4 rotation;


XDRFILE * xdrFile;
GLint NbAtoms;
GLfloat bbox[6];
GLfloat centre[3];

#define X .525731112119133606
#define Z .850650808352039932    

// Le premier niveau : l'icosaèdre le plus petit pour représenter une sphère

static GLfloat ico_sommets[36] =
  {
    -X, 0, Z,
    X, 0, Z,
    -X, 0, -Z,
    X, 0, -Z,
    0, Z, X,
    0, Z, -X,
    0, -Z, X,
    0, -Z, -X,
    Z, X, 0,
    -Z, X, 0,
    Z, -X, 0,
    -Z, -X, 0
  };

// Le tableau d'indices correspondant
GLuint ico_indices[60] =
  {
    0, 4, 1,
    0, 9, 4,
    9, 5, 4,
    4, 5, 8,
    4, 8, 1,
    8, 10, 1,
    8, 3, 10,
    5, 3, 8,
    5, 2, 3,
    2, 7, 3,
    7, 10, 3,
    7, 6, 10,
    7, 11, 6,
    11, 0, 6,
    0, 1, 6,
    6, 1, 10,
    9, 0, 11,
    9, 11, 2,
    9, 2, 5,
    7, 2, 11
  };

void get_first_frame()
{
  bool eof = false;
  int step;
  float tps;
  float box[9];
  if (!eof) {
    if (xdrfile_getframe_header(&NbAtoms, &step, &tps, box, xdrFile)!=1) {
      int size = 12;
      float * oldsommets = new float[3*NbAtoms];
      sommets = new float[3*NbAtoms];
      couleurs = new float[3*NbAtoms];    
      
      char c;  

      ifstream f;
      float rgb[3];
      f.open("gestioncouleur.txt", ios::in);      
      xdrfile_getframe_positions(NbAtoms, oldsommets, xdrFile);

      int index = 0;
      int sommet = 0;
      f >> c;
      for (; !f.eof(); index += 3, sommet += 3 ) {

	choix_couleur(c, rgb);
	couleurs[index] = rgb[0];
	couleurs[index + 1] = rgb[1];
	couleurs[index + 2] = rgb[2];	  
	sommets[index] = oldsommets[sommet];	  
	sommets[index + 1] = oldsommets[sommet + 1];//oldsommets[i*3+1];
	sommets[index + 2] = oldsommets[sommet + 2];//oldsommets[i*3+2];

	f>>c;
      }
      
      f.close();
      //subdivision(oldsommets,sommets);
      delete [] oldsommets;
      bbox[0] = sommets[0];
      bbox[1] = sommets[1];
      bbox[2] = sommets[2];
      bbox[3] = sommets[0];
      bbox[4] = sommets[1];
      bbox[5] = sommets[2];
      for (int i=1; i<NbAtoms; i++) {
	if (sommets[3*i]<bbox[0])
	  bbox[0] = sommets[3*i];
	if (sommets[3*i+1]<bbox[1])
	  bbox[1] = sommets[3*i+1];
	if (sommets[3*i+2]<bbox[2])
	  bbox[2] = sommets[3*i+2];
	if (sommets[3*i]>bbox[3])
	  bbox[3] = sommets[3*i];
	if (sommets[3*i+1]>bbox[4])
	  bbox[4] = sommets[3*i+1];
	if (sommets[3*i+2]>bbox[5])
	  bbox[5] = sommets[3*i+2];
      }
    }
    centre[0] = bbox[0]+(bbox[3]-bbox[0])/2;
    centre[1] = bbox[1]+(bbox[4]-bbox[1])/2;
    centre[2] = bbox[2]+(bbox[5]-bbox[2])/2;
  }

}

void get_frame()
{
  int step;
  float tps;
  float* box;

  if (xdrfile_getframe_header(&NbAtoms, &step, &tps, box, xdrFile)!=1)  
    xdrfile_getframe_positions(NbAtoms, sommets, xdrFile);       
  else {
    xdrfile_close(xdrFile);
    xdrFile = xdrfile_open("00_peptide.200_traj1.xtc","r");
  }
}


void init() {
  
  
  glClearColor(0.0f, 0.0f, 0.5f, 0.0f);

  glClearDepth(1.0);
  glEnable(GL_DEPTH_TEST);

  programID = LoadShaders( "VertexShader.vert", "FragmentShader.frag");

  get_first_frame();
 
  glGenBuffers(2,vboid);
  glBindBuffer(GL_ARRAY_BUFFER, vboid[0]);
  glBufferData(GL_ARRAY_BUFFER, 12 * 3 * sizeof(float), ico_sommets, GL_STATIC_DRAW);
  
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboid[1]);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, 20 * 3 * sizeof(unsigned int), ico_indices, GL_STATIC_DRAW);

  positionID = glGetUniformLocation(programID, "position");
  colorID = glGetUniformLocation(programID, "color");

  MatrixID = glGetUniformLocation(programID, "MVP");
  
  Projection = glm::perspective(70.0f, 4.0f / 3.0f, 0.1f, 1000.0f);
  View = glm::lookAt(glm::vec3(0,0,-5), glm::vec3(0,0,0), glm::vec3(0,1,0));
  
  trans_initial = glm::translate(glm::mat4(1.0f), glm::vec3(-centre[0],-centre[1],-centre[2]));
  
  Model = glm::mat4(1.0f);

  MVP = Projection * View * Model; 
  
  rotation = glm::mat4(1.0f);
  
  translation = glm::translate(glm::mat4(1.0f), glm::vec3(0,0,35));
}

void Display(void) {
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glUseProgram(programID);

  Model = translation * rotation;
  MVP =  Projection * View * Model;

  glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);

  for(int i = 0 ; i < NbAtoms; i++) {

    glUniform3f(positionID, sommets[i * 3], sommets[i * 3 + 1], sommets[i * 3 + 2]);			 
    glUniform3f(colorID, couleurs[i * 3], couleurs[i * 3 + 1], couleurs[i * 3 + 2]);
    
    glBindBuffer(GL_ARRAY_BUFFER, vboid[0]);
    glVertexPointer(3,GL_FLOAT,3 * sizeof(GLfloat),(void*)0);    
    
    glEnableClientState(GL_VERTEX_ARRAY); 
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboid[1]);
    glDrawElements(GL_TRIANGLES,20*3,GL_UNSIGNED_INT,0);
  }
  
  glDisableClientState(GL_VERTEX_ARRAY);
  glutSwapBuffers();
}

void Idle()
{
  //get_frame();
  // glBindBuffer(GL_ARRAY_BUFFER, vboid[0]);
  //glBufferData(GL_ARRAY_BUFFER,3*NbAtoms*sizeof(float),sommets,GL_DYNAMIC_DRAW);
  //glutPostRedisplay();
}

void ClavierClassique(unsigned char key, int x, int y)
{
  switch(key) {	
  case 0x1B:
    exit(0);
  }
}

void ClavierSpecial(int key, int x, int y)
{
  switch(key) {
  case GLUT_KEY_UP:
    translation = glm::translate(translation, glm::vec3(0.f, stepTrans, 0.f));
    break;
  case GLUT_KEY_DOWN:
    translation = glm::translate(translation, glm::vec3(0.f, -stepTrans, 0.f));
    break;
  case GLUT_KEY_RIGHT:
    translation = glm::translate(translation, glm::vec3(stepTrans, 0.f, 0.f));
    break;
  case GLUT_KEY_LEFT:
    translation = glm::translate(translation, glm::vec3(-stepTrans, 0.f, 0.f));
    break;
  case GLUT_KEY_PAGE_UP:
    translation = glm::translate(translation, glm::vec3(0.f, 0.f, stepTrans));
    break;
  case GLUT_KEY_PAGE_DOWN:
    translation = glm::translate(translation, glm::vec3(0.f, 0.f, -stepTrans));
    break;
  }
  glutPostRedisplay();  
}

void Souris(int bouton, int etat, int x, int y)
{	  
  switch (bouton) {
  case GLUT_LEFT_BUTTON :
    if (etat==GLUT_DOWN) {
      leftbutton=true;
      mouseXOld = x;
      mouseYOld = y;
    }
    else
      leftbutton=false;
    break;
  case GLUT_MIDDLE_BUTTON :
    if (etat==GLUT_DOWN) {
      middlebutton=true;
      mouseXOld = x;
      mouseYOld = y;
    }
    else
      middlebutton=false;
    break;
  case GLUT_RIGHT_BUTTON:
    if (etat==GLUT_DOWN) {
      rightbutton=true;
      mouseXOld = x;
      mouseYOld = y;
    }
    else
      rightbutton=false;
    break;
  }
  glutPostRedisplay();  
}  
void Motion (int x, int y)
{
  if (middlebutton) {
    if (abs(y-mouseYOld) > abs(x-mouseXOld)) 
      translation = glm::translate(translation, glm::vec3(0.f, 0.f, -(y-mouseYOld)*stepTrans/10));
    else 
      translation = glm::translate(translation, glm::vec3(0.f, 0.f, -(x-mouseYOld)*stepTrans/10));
    mouseXOld=x;
    mouseYOld=y;
  }
  else if (rightbutton) {
    translation = glm::translate(translation, glm::vec3(-(x-mouseXOld)*stepTrans/10, -(y-mouseYOld)*stepTrans/10, 0.f));
    mouseXOld=x;
    mouseYOld=y;
  }
   
  if (leftbutton){
    rotation = glm::rotate(rotation, (x-mouseXOld)*stepTrans, glm::vec3(0.f, 1.f, 0.f));
    rotation = glm::rotate(rotation, -(y-mouseXOld)*stepTrans, glm::vec3(1.f, 0.f, 0.f));
    mouseXOld=x;
    mouseYOld=y;
  }
  glutPostRedisplay();     
}

int main(int argc, char** argv)
{
  xdrFile = xdrfile_open("00_peptide.200_traj1.xtc","r"); //Ouverture du fichier en lecture

  glutInit (&argc,argv) ;
  glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH) ;
  glutInitWindowSize (width,height) ;
  glutInitWindowPosition (100, 100) ;
  glutCreateWindow ("GLUT") ;
  GLenum err = glewInit();

  init();
  glutDisplayFunc(Display);
  glutIdleFunc(Idle);
  glutKeyboardFunc(ClavierClassique);
  glutSpecialFunc(ClavierSpecial);
  glutMouseFunc(Souris);
  glutMotionFunc(Motion);

  glutMainLoop () ;

  xdrfile_close(xdrFile);
  return 0 ;
  
}

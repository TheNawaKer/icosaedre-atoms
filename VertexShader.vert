#version 130

uniform mat4 MVP;
uniform vec3 color;
uniform vec3 position;

out vec4 out_color;

void main() {
     out_color = vec4(color, 1.0);
     gl_Position = MVP * (gl_Vertex + vec4(position, 1.0));	
}

#ifndef ICOSAEDRE_HPP
#define ICOSAEDRE_HPP

void normalisation(GLfloat* v);
void subdivision(GLfloat* sommets, GLfloat* newsommets);

#endif

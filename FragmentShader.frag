#version 130

in vec4 out_color;

void main() {
  gl_FragColor = out_color;
}

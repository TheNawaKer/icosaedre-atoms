## Compilation automatique de source C
# Compilteur
CC := g++
#options de compilation
CFLAGS := -g --std=c++98 

LIBS = -lGL -lglut -lm -lGLU -lGLEW -lpthread
LIBSDIR = -L/usr/X11R6/lib 
INCLDIR = -I/usr/include/GL

# Règle de compilation
all : code_icosaedre

code_icosaedre : code_icosaedre.o lecture_trajectoire.o icosaedre.o shaders_utilities.o xdrfile.o
	$(CC) $(CFLAGS) $(LIBSDIR) -o $@ $^ $(LIBS)	

xdrfile.o :
	gcc -c -o xdrfile.o xdrfile.c

%.o: %.cpp
	$(CC) $(CFLAGS) $(INCLDIR) -o $@ -c $< 


clean:
	rm *.o

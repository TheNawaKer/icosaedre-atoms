#extension GL_EXT_geometry_shader4 : enable
 
void main(){
  for (int i = 0; i < gl_VerticesIn; ++i)
  {
    gl_Position = gl_PositionIn[i];
    EmitVertex ();
  }
  EndPrimitive ();
  
  for (int i = 0; i < gl_VerticesIn; ++i)
  {
    gl_Position = gl_PositionIn[i]+vec4(50,50,50,1);
    EmitVertex ();
  }
  EndPrimitive ();
}
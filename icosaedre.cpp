#include <iostream>

#include <GL/glut.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <math.h>
#include "icosaedre.hpp"

using namespace std;

void normalisation(GLfloat* v)
{
  float d=sqrt(v[0]*v[0]+v[1]*v[1]+v[2]*v[2]);
   v[0]=v[0]/d;
   v[1]=v[1]/d;
   v[2]=v[2]/d;
  
}

void subdivision(GLfloat* sommets, GLfloat* newsommets)
{
  float v1[3];
  float v2[3];
  float v3[3];
  float v4[3];
  float v5[3];
  float v6[3];
  for (int j=0; j<3; j++){
    v1[j] = sommets[j];
    v2[j] = sommets[3+j];
    v3[j] = sommets[3*2+j];
  }
  
  for (int j=0; j<3; j++){
    v4[j] = (v1[j]+v2[j])/2;
    v5[j] = (v1[j]+v3[j])/2;
    v6[j] = (v2[j]+v3[j])/2;
  }
  
  normalisation(v4);
  normalisation(v5);
  normalisation(v6);
  
  for (int j=0; j<3; j++) 
    newsommets[j] = v1[j];
  for (int j=0; j<3; j++) 
    newsommets[3 + j] = v2[j]; 
  for (int j=0; j<3; j++) 
    newsommets[6 + j] = v3[j]; 
  for (int j=0; j<3; j++) 
    newsommets[9 + j] = v4[j]; 
  for (int j=0; j<3; j++) 
    newsommets[12 + j] = v5[j]; 
  for (int j=0; j<3; j++) 
    newsommets[15 + j] = v6[j]; 
  
}




